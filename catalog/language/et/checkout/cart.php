<?php
// Heading
$_['heading_title']            = 'Pakkumine';
$_['text_shopping_cart_title'] = 'Pakkumine';

// Text
$_['text_success']             = '<a href="%s">%s</a> lisatud sinu <a href="%s">pakkumisele</a>!';
$_['text_remove']              = 'Pakkumise kogused muudetud!';
$_['text_login']               = 'Tähelepanu: pead olema <a href="%s">sisse logitud</a> või <a href="%s">registreerima konto</a>, et hindu näha!';
$_['text_items']               = '%s toode(t) - %s';
$_['text_points']              = 'Boonuspunktid: %s';
$_['text_next']                = 'Mida soovid nüüd teha?';
$_['text_next_choice']         = 'Vali kui sul on allahindluskood või boonuspunkte, mida soovid kasutada.';
$_['text_empty']               = 'Pakkumine on tühi!';
$_['text_day']                 = 'päev';
$_['text_week']                = 'nädal';
$_['text_semi_month']          = 'poolkuu';
$_['text_month']               = 'kuu';
$_['text_year']                = 'aasta';
$_['text_trial']               = '%s every %s %s for %s payments then ';
$_['text_recurring']           = '%s every %s %s';
$_['text_payment_cancel']      = 'kuni tühistatud';
$_['text_recurring_item']      = 'Recurring Item';
$_['text_payment_recurring']   = 'Payment Profile';
$_['text_trial_description']   = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description'] = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']      = '%s every %d %s(s) until canceled';
$_['button_update_cart']       = 'Uuenda';
$_['button_remove_item']       = 'Eemalda';
$_['text_related_cart']        = 'Soovitatud koos nende toodetega';

// Input
$_['text_input_name']          = 'Tellija / Ettevõtte nimi';
$_['text_input_email']         = 'E-post';
$_['text_input_phone']         = 'Telefon';
$_['text_input_transport']     = 'Transport';
$_['text_radio_pickup']        = 'Tulen ise kaubale järele';
$_['text_radio_delivery']      = 'Soovin transpordi hinnapakkumist';
$_['text_input_address']       = 'Aadress';
$_['text_input_comment']       = 'Lisainfo';
$_['text_input_comment_placeholder'] = 'Lisainfo';
$_['button_submit_order']      = 'Saada päring';
$_['text_send_confirm']        = 'Saada päring?';
$_['text_send_confirm_2']      = 'Kas kontrollisid üle, et pakkumises on kõik ja ainult need tooted, mida soovid tellida?';
$_['button_send']              = 'Saada';
$_['button_cancel_send']       = 'Tühista';

// Success
$_['text_order_products']	   = 'Tooted';
$_['email_subject']            = '%s on esitanud päringu';
$_['email_subject_2']          = 'Päring %s veebipoest';
$_['order_success']            = 'Päring õnnestus';
$_['text_order_success_message'] = 'Teie päring on edastatud. Küsimuste või probleemide korral palun võtke meiega ühendust.';
$_['text_disclaimer']		   = '';

// Column
$_['column_image']             = 'Pilt';
$_['column_name']              = 'Toote nimi';
$_['column_model']             = 'Tootekood';
$_['column_quantity']          = 'Kogus';
$_['column_price']             = 'Ühiku hind';
$_['column_total']             = 'Kokku';

// Error
$_['error_stock']              = 'Tooted märgitud *** ei ole saadaval soovitud koguses!';
$_['error_minimum']            = 'Minimaalne tellimuse summa %s on %s!';
$_['error_required']           = '%s on nõutud!';
$_['error_product']            = 'Hoiatus: Pakkumine on tühi!';
$_['error_recurring_required'] = 'Palun vali korduv makse!';
$_['text_error']               = 'Viga!';
$_['text_error_name']          = 'Nimi on nõutud';
$_['text_error_email']         = 'Vigane e-posti aadress';
$_['text_error_phone']         = 'Telefoni number on nõutud';
$_['text_error_transport']     = 'Transpordi valik on nõutud';
$_['text_error_address']       = 'Aadress on nõutud';
