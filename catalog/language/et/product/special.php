<?php
// Heading
$_['heading_title']     = 'Sooduspakkumised';

// Text
$_['text_empty']        = 'Ühtegi sooduspakkumist ei leitud.';
$_['text_quantity']     = 'Kogus:';
$_['text_manufacturer'] = 'Tootja:';
$_['text_model']        = 'Tootekood:';
$_['text_points']       = 'Boonuspunktid:';
$_['text_price']        = 'Hind:';
$_['text_tax']          = 'Maksudeta:';
$_['text_compare']      = 'Tootevõrdlus (%s)';
$_['text_sort']         = 'Sorteeri:';
$_['text_default']      = 'Vaikimisi';
$_['text_name_asc']     = 'Nimi (A - Z)';
$_['text_name_desc']    = 'Nimi (Z - A)';
$_['text_price_asc']    = 'Hind (alates odavamast)';
$_['text_price_desc']   = 'Hind (alates kallimast)';
$_['text_rating_asc']   = 'Hinnang (Madalamast)';
$_['text_rating_desc']  = 'Hinnang (Kõrgemast)';
$_['text_model_asc']    = 'Tootekood (A - Z)';
$_['text_model_desc']   = 'Tootekood (Z - A)';
$_['text_limit']        = 'Näita:';