<?php
// Text
$_['text_refine']       = 'Täpsusta kategooria';
$_['text_product']      = 'Tooted';
$_['text_error']        = 'Kategooriat ei leitud!';
$_['text_empty']        = 'Sellest kategooriast ei leitud ühtegi toodet.';
$_['text_quantity']     = 'Kogus:';
$_['text_manufacturer'] = 'Tootja:';
$_['text_model']        = 'Toote kood:';
$_['text_points']       = 'Boonuspunktid:';
$_['text_price']        = 'Hind:';
$_['text_tax']          = 'Lisandub KM (20%)';
$_['text_compare']      = 'Tootevõrdlus (%s)';
$_['text_sort']         = 'Sorteeri:';
$_['text_default']      = 'Vaikimisi';
$_['text_name_asc']     = 'Nimi (A - Z)';
$_['text_name_desc']    = 'Nimi (Z - A)';
$_['text_price_asc']    = 'Hind (alates odavamast)';
$_['text_price_desc']   = 'Hind (alates kallimast)';
$_['text_rating_asc']   = 'Hinnang (Madalamast)';
$_['text_rating_desc']  = 'Hinnang (Kõrgemast)';
$_['text_model_asc']    = 'Tootekood (A - Z)';
$_['text_model_desc']   = 'Tootekood (Z - A)';
$_['text_limit']        = 'Näita:';