<?php
// Heading
$_['heading_title']     = 'Otsing';
$_['heading_tag']       = 'Märksõna - ';

// Text
$_['text_search']       = 'Otsingutulemused';
$_['text_search_results'] = 'Otsingutulemused';
$_['text_keyword']      = 'Märksõnad';
$_['text_category']     = 'Kõik kategooriad';
$_['text_all_categories'] = 'Kõik kategooriad';
$_['text_sub_category'] = 'Otsi alamkategooriatest';
$_['text_empty']        = 'Ühtegi toodet ei leitud.';
$_['text_no_products']  = 'Ühtegi toodet ei leitud.';
$_['text_quantity']     = 'Kogus:';
$_['text_manufacturer'] = 'Tootja:';
$_['text_model']        = 'Tootekood:';
$_['text_points']       = 'Boonuspunktid:';
$_['text_price']        = 'Hind:';
$_['text_tax']          = 'Maksudeta:';
$_['text_reviews']      = '%s kommentaari põhjal.';
$_['text_compare']      = 'Tootevõrdlus (%s)';
$_['text_sort']         = 'Sorteeri:';
$_['text_default']      = 'Vaikimisi';
$_['text_name_asc']     = 'Nimi (A - Z)';
$_['text_name_desc']    = 'Nimi (Z - A)';
$_['text_price_asc']    = 'Hind (alates odavamast)';
$_['text_price_desc']   = 'Hind (alates kallimast)';
$_['text_rating_asc']   = 'Hinnang (Madalamast)';
$_['text_rating_desc']  = 'Hinnang (Kõrgemast)';
$_['text_model_asc']    = 'Tootekood (A - Z)';
$_['text_model_desc']   = 'Tootekood (Z - A)';
$_['text_limit']        = 'Näita:';

// Entry
$_['entry_search']      = 'Otsi...';
$_['entry_description'] = 'Otsi tootekirjeldustest';