<?php
// Heading
$_['heading_title']     = 'Tootevõrdlus';

// Text
$_['text_product']      = 'Toote andmed';
$_['text_name']         = 'Toode';
$_['text_image']        = 'Pilt';
$_['text_price']        = 'Hind';
$_['text_model']        = 'Mudel';
$_['text_manufacturer'] = 'Bränd';
$_['text_availability'] = 'Saadavus';
$_['text_instock']      = 'Laos';
$_['text_rating']       = 'Hinnang';
$_['text_reviews']      = 'Based on %s reviews.';
$_['text_summary']      = 'Kokkuvõte';
$_['text_weight']       = 'Kaal';
$_['text_dimension']    = 'Mõõdud (pakendis)';
$_['text_compare']      = 'Product Compare (%s)';
$_['text_success']      = 'Lisasid <a href="%s">%s</a> <a href="%s">tootervõrdlusesse</a>!';
$_['text_remove']       = 'Tootevõrdlust muudetud!';
$_['text_empty']        = 'Te pole valinud võrdlemiseks tooteid';