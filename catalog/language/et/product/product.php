<?php
// Text
$_['text_categories']		   = 'Kategooriad:';
$_['text_search']              = 'Otsi';
$_['text_brand']               = 'Kaubamärk';
$_['text_manufacturer']        = 'Tootja:';
$_['text_tootja'] 		       = 'Tootja:';
$_['text_model']               = 'Tootekood:';
$_['text_reward']              = 'Boonuspunktid:';
$_['text_points']              = 'Hind boonuspunktides:';
$_['text_stock']               = 'Saadavus:';
$_['text_instock']             = 'Laos';
$_['text_tax']                 = 'Lisandub KM (20%)';
$_['text_discount']            = ' või rohkem ';
$_['text_option']              = 'Võimalikud valikud';
$_['text_minimum']             = 'Toote minimaalne tellimiskogus on %s';
$_['text_reviews']             = '%s kommentaari';
$_['text_write']               = 'Kirjuta kommentaar';
$_['text_login']               = 'Kommenteerimiseks palun <a href="%s">logi sisse</a> või <a href="%s">registreeri</a>';
$_['text_no_reviews']          = 'Kommentaare ei ole.';
$_['text_note']                = '<span class="text-danger">NB:</span> HTML koodi ei saa sisestada!';
$_['text_success']             = 'Täname kommentaari eest. Kommentaar on edastatud ülevaatuseks';
$_['text_related']             = 'Seotud tooted';
$_['text_tags']                = 'Märksõnad:';
$_['text_error']               = 'Toodet ei leitud!';
$_['text_payment_recurring']   = 'Maksmisprofiil';
$_['text_trial_description']   = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description'] = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']      = '%s every %d %s(s) until canceled';
$_['text_day']                 = 'päev';
$_['text_week']                = 'nädal';
$_['text_semi_month']          = 'poolkuu';
$_['text_month']               = 'kuu';
$_['text_year']                = 'aasta';
$_['text_cart_extra']          = 'Lisage toode korvi, et saada pakkumine vajaminevale kogusele.';

// Entry
$_['entry_qty']                = 'Kogus';
$_['entry_name']               = 'Nimi';
$_['entry_review']             = 'Kommentaar';
$_['entry_rating']             = 'Hinnang';
$_['entry_good']               = '<i class="fa fa-thumbs-o-up fa-lg"></i>';
$_['entry_bad']                = '<i class="fa fa-thumbs-o-down fa-lg"></i>';

// Tabs
$_['tab_description']          = 'Kirjeldus';
$_['tab_general']              = 'Üldandmed';
$_['tab_material']             = 'Materjal';
$_['tab_instructions']         = 'Juhendid';
$_['tab_video']                = 'Video';
$_['tab_attribute']            = 'Lisainfo';
$_['tab_download']             = 'Failid';
$_['tab_review']               = 'Kommentaarid (%s)';

// Error
$_['error_name']               = 'Hoiatus: Nimi peab olema 3 kuni 25 tähemärki!';
$_['error_text']               = 'Hoiatus: Kommentaar peab olema 25 kuni 1000 tähemärki!';
$_['error_rating']             = 'Hoiatus: Palun märgi hinnang!';