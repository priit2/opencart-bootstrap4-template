<?php
// Heading
$_['heading_title'] = 'Logi välja';

// Text
$_['text_message']  = '<p>Olete välja logitud. Ohutuse tagamiseks palun sulgege brauseri aken.</p><p>Sinu ostukorv on salvestatud, järgmine kord sisse logides saate jätkata, kus pooleli jäite.</p>';
$_['text_account']  = 'Konto';
$_['text_logout']   = 'Logi välja';