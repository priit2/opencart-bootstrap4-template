<?php
// Heading
$_['heading_title']       = 'Minu konto';

// Text
$_['text_account']        = 'Konto';
$_['text_my_account']     = 'Minu konto';
$_['text_my_orders']      = 'Minu tellimused';
$_['text_my_affiliate']   = 'Minu <i>affiliate</i> konto';
$_['text_my_newsletter']  = 'Uudiskiri';
$_['text_edit']           = 'Muuda oma konto andmeid';
$_['text_password']       = 'Muuda oma parooli';
$_['text_address']        = 'Muuda oma aadressiraamatu sissekandeid';
$_['text_credit_card']    = 'Halda salvestatud krediitkaarte';
$_['text_wishlist']       = 'Muuda oma ostunimekirja';
$_['text_order']          = 'Vaata oma tellimuste ajalugu';
$_['text_download']       = 'Allalaadimised';
$_['text_reward']         = 'Sinu boonuspunktid';
$_['text_return']         = 'Vaata oma tagastuste soove';
$_['text_transaction']    = 'Minu tehingud';
$_['text_newsletter']     = 'Liitu / loobu uudiskirjast';
$_['text_recurring']      = 'Korduvad maksed';
$_['text_transactions']   = 'Tehingud';
$_['text_affiliate_add']  = 'Registreeri <i>affiliate</i> konto';
$_['text_affiliate_edit'] = 'Muuda oma <i>affiliate</i> andmeid';
$_['text_tracking']       = '<i>Affiliate</i> jälgimiskood';