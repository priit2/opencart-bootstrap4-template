<?php
// Heading
$_['heading_title']    = 'Uudiskiri';

// Text
$_['text_account']     = 'Konto';
$_['text_newsletter']  = 'Uudiskiri';
$_['text_success']     = 'Olete liitunud uudiskirjaga!';

// Entry
$_['entry_newsletter'] = 'Liitu';