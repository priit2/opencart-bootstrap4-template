<?php
// Heading
$_['heading_title']   = 'Unustasid parooli?';

// Text
$_['text_account']    = 'Konto';
$_['text_forgotten']  = 'Unustatud parool';
$_['text_your_email'] = 'E-posti aadress';
$_['text_email']      = 'Sisesta e-posti aadress, millega registreerusid ja me saadame juhendi parooli taastamiseks sellele e-posti aadressile.';
$_['text_success']    = 'Saatsime antud e-posti aadressile kinnituskirja.';

// Entry
$_['entry_email']     = 'E-posti aadress';
$_['entry_password']  = 'Uus parool';
$_['entry_confirm']   = 'Kinnita uus parool';

// Error
$_['error_email']     = 'Hoiatus: Sellist e-posti aadressi ei leitud meie andmebaasist, palun proovige uuesti!';
$_['error_approved']  = 'Hoiatus: Sinu konto vajab kinnitamist ennem kui saad sisse logida.';
$_['error_password']  = 'Parool peab olema 4 kuni 20 tähemärki!';
$_['error_confirm']   = 'Paroolid ei kattu!';