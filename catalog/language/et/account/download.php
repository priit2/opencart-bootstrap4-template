<?php
// Heading
$_['heading_title']     = 'Allalaadimised';

// Text
$_['text_account']      = 'Konto';
$_['text_downloads']    = 'Allalaadimised';
$_['text_empty']        = 'Sul ei ole allalaadimistega tellimusi!';

// Column
$_['column_order_id']   = 'Tellimuse ID';
$_['column_name']       = 'Nimi';
$_['column_size']       = 'Suurus';
$_['column_date_added'] = 'Kuupäev';