<?php
// Heading
$_['heading_title']      = 'Aadressiraamat';

// Text
$_['text_account']       = 'Konto';
$_['text_address_book']  = 'Aadressiraamatu sissekanded';
$_['text_address_add']   = 'Lisa aadress';
$_['text_address_edit']  = 'Muuda aadressi';
$_['text_add']           = 'Aadress edukalt lisatud';
$_['text_edit']          = 'Aadress edukalt muudetud';
$_['text_delete']        = 'Aadress kustutatud';
$_['text_empty']         = 'Sinu kontol pole ühtegi aadressi seadistatud.';

// Entry
$_['entry_firstname']    = 'Eesnimi';
$_['entry_lastname']     = 'Perekonnanimi';
$_['entry_company']      = 'Firma';
$_['entry_address_1']    = 'Aadress 1';
$_['entry_address_2']    = 'Aadress 2';
$_['entry_postcode']     = 'Postiindeks';
$_['entry_city']         = 'Linn';
$_['entry_country']      = 'Riik';
$_['entry_zone']         = 'Maakond';
$_['entry_default']      = 'Vaikimisi aadress';

// Error
$_['error_delete']       = 'Hoiatus: Sul peab olema vähemalt üks aadress!';
$_['error_default']      = 'Hoiatus: Sa ei saa kustutada oma vaikimisi aadressi!';
$_['error_firstname']    = 'Eesnimi peab olema 1 kuni 32 tähemärki!';
$_['error_lastname']     = 'Prekonnanimi peab olema 1 kuni 32 tähemärki!';
$_['error_address_1']    = 'Aadress peab olema 3 kuni 128 tähemärki!';
$_['error_postcode']     = 'Postiindeks peab olema 2 kuni 10 tähemärki!';
$_['error_city']         = 'Linn peab olema 2 kuni 128 tähemärki!';
$_['error_country']      = 'Palun vali riik!';
$_['error_zone']         = 'Palun vali maakond!';
$_['error_custom_field'] = '%s on nõutud!';