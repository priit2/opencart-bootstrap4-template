<?php
// Heading
$_['heading_title']                = 'Logi sisse';

// Text
$_['text_account']                 = 'Konto';
$_['text_login']                   = 'Logi sisse';
$_['text_new_customer']            = 'Uus klient';
$_['text_register']                = 'Registreeri konto';
$_['text_register_account']        = '';
$_['text_returning_customer']      = 'Oman kontot';
$_['text_i_am_returning_customer'] = 'Mul on juba konto';
$_['text_forgotten']               = 'Unustasin parooli';

// Entry
$_['entry_email']                  = 'E-posti aadress';
$_['entry_password']               = 'Parool';

// Error
$_['error_login']                  = 'Hoiatus: Sellise e-posti aadressi või parooliga kasutajat ei leitud.';
$_['error_attempts']               = 'Hoiatus: Sinu konto on ületanud sisselogimiskatsete arvu. Palun proovi uuesti tunni aja pärast.';
$_['error_approved']               = 'Hoiatus: Sinu konto vajab kinnitamist ennem kui saad sisse logida.';