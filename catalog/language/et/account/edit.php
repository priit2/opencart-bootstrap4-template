<?php
// Heading
$_['heading_title']      = 'Minu konto info';

// Text
$_['text_account']       = 'Konto';
$_['text_edit']          = 'Muuda infot';
$_['text_your_details']  = 'Minu personaalsed andmed';
$_['text_success']       = 'Sinu konto on uuendatud.';

// Entry
$_['entry_firstname']    = 'Eesnimi';
$_['entry_lastname']     = 'Perekonnanimi';
$_['entry_email']        = 'E-post';
$_['entry_telephone']    = 'Telefon';

// Error
$_['error_exists']       = 'Hoiatus: E-posti aadress on juba registreeritud!';
$_['error_firstname']    = 'Eesnimi peab olema 1 kuni 32 tähemärki!';
$_['error_lastname']     = 'Prekonnanimi peab olema 1 kuni 32 tähemärki!';
$_['error_email']        = 'E-posti aadress tundub vigane!';
$_['error_telephone']    = 'Telefon peab olema 3 kuni 32 tähemärki!';
$_['error_custom_field'] = '%s on nõutud!';