<?php
// Text
$_['text_information']  = 'Info';
$_['text_service']      = 'Klienditeenindus';
$_['text_extra']        = 'Lisad';
$_['text_contact']      = 'Kontakt';
$_['text_return']       = 'Tagastused';
$_['text_sitemap']      = 'Sisukaart';
$_['text_manufacturer'] = 'Tootjad';
$_['text_voucher']      = 'Kinkekaardid';
$_['text_affiliate']    = 'Affiliate';
$_['text_special']      = 'Sooduspakkumised';
$_['text_account']      = 'Minu konto';
$_['text_order']        = 'Tellimuste ajalugu';
$_['text_wishlist']     = 'Ostunimekiri';
$_['text_newsletter']   = 'Uudiskiri';
$_['text_powered']      = '%s &copy; %s';
$_['text_added_to_cart'] = 'lisatud pakkumisele!';
$_['text_added_to_wishlist'] = 'lisatud ostunimekirja!';
$_['text_get_in_touch'] = 'Võtke ühendust!';
$_['text_go_to_cart'] = 'Pakkumine';