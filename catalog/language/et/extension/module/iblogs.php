<?php
$_['text_blog_search']      = 'Blogi otsing: ';
$_['text_blog_tags']        = 'Blogi sildid: ';

$_['text_rss_feed']         = 'Blog RSS Feed';
$_['text_read_more']        = 'Loe edasi';
$_['text_search']           = 'Otsi: ';
$_['text_tags']             = 'Sildid: ';
$_['text_related_post']     = 'Seotud postitused';
$_['text_related_product']  = 'Seotud tooted';
$_['text_comments']         = 'Kommentaarid';

$_['text_recent']           = 'Hiljutised';
$_['text_popular']          = 'Populaarsed';
$_['text_tag']              = 'Sildid';
$_['text_tax']              = 'Maksudeta:';

$_['button_cart']           = 'Lisa pakkumisele';

$_['text_not_found']        = 'Ei leitud';
$_['text_no_post']          = 'Postitust ei leitud';
