<?php
// Heading
$_['heading_title']    = 'Sisukaart';

// Text
$_['text_special']     = 'Sooduspakkumised';
$_['text_account']     = 'Konto';
$_['text_edit']        = 'Muuda kontot';
$_['text_password']    = 'Parool';
$_['text_address']     = 'Aadressid';
$_['text_history']     = 'Tellimuste ajalugu';
$_['text_download']    = 'Allalaadimised';
$_['text_cart']        = 'Pakkumine';
$_['text_shoppingcart'] = 'Pakkumine';
$_['text_checkout']    = 'Maksmine';
$_['text_search']      = 'Otsing';
$_['text_search_page'] = 'Otsing';
$_['text_information'] = 'Info';
$_['text_contact']     = 'Kontakt';
$_['text_manufacturers'] = 'Tootjad';