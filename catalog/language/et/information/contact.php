<?php
// Heading
$_['heading_title']  = 'Kontakt';
$_['title_contact'] = 'Kontakt';
$_['contact_success'] = 'Sõnum edastatud';
// Text
$_['text_location']  = 'Firma kontaktid';
$_['text_store']     = 'Hutton kauplused';
$_['text_contact']   = 'Kontakt';
$_['text_map']	  	 = 'Kaart';
$_['text_contact_form'] = 'Kontaktivorm';
$_['text_address']   = 'Aadress';
$_['text_telephone'] = 'Telefon';
$_['text_fax']       = 'Faks';
$_['text_open']      = 'Avatud';
$_['text_comment']   = 'Lisainfo';
$_['text_success']   = '<p>Sõnum on edastatud! Vastame 24h jooksul!</p>';
$_['text_message']   = '<p>Sõnum on edastatud! Vastame 24h jooksul!</p>';

// Entry
$_['entry_name']     = 'Nimi (eesnimi või objekti aadress)';
$_['entry_email']    = 'E-post (suhtlemiseks toimiv e-posti aadress)';
$_['entry_phone']    = 'Telefoni number (korrektne telefoninumber nõutav, numbri kuju 5011324)';
$_['entry_enquiry']  = 'Sõnum (tekst kuni 3000 tähemärki)';
$_['entry_file']     = 'Lisa fail';
$_['entry_upload']   = 'Vali fail';

// Email
$_['email_subject']  = 'Sõnum %s';

// Errors
$_['error_name']     = 'Nimi peab olema 3 kuni 32 tähemärki!';
$_['error_email']    = 'E-posti aadress ei tundu korrektne olevat!';
$_['error_phone']    = 'Telefoninumber on kohustuslik kujul 5011324';
$_['error_enquiry']  = 'Sõnum peab jääma 10 kuni 3000 tähemärgi vahele!';