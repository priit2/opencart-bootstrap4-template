<?php
// Heading
$_['heading_title']  = 'Koolitusele registreerimine';
$_['title_contact']  = 'Andmed';
$_['contact_success'] = 'Registreerimine edastatud';

$_['text_success']   = '<p>Registreerimine on edastatud! Võtame ühendust täiendava info jaoks.</p>';
$_['text_message']   = '<p>Registreerimine on edastatud! Võtame ühendust täiendava info jaoks.</p>';

// Entry
$_['entry_training'] = 'Koolitus';
$_['entry_name']     = 'Nimi';
$_['entry_email']    = 'E-post';
$_['entry_phone']    = 'Telefon';
$_['entry_enquiry']  = 'Sõnum';

// Email
$_['email_subject']  = '%s soovib registreeruda koolitusele %s';

// Errors
$_['error_name']     = 'Nimi peab olema 3 kuni 32 tähemärki!';
$_['error_email']    = 'E-posti aadress ei tundu korrektne olevat!';
$_['error_phone']    = 'Telefoninumber on kohustuslik!';
$_['error_enquiry']  = 'Sõnum peab jääma 10 kuni 3000 tähemärgi vahele!';