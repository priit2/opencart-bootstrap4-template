This Bootstrap4 template for Opencart was made by Idyllum Labs & Elixir OÜ & Kookos UÜ in October 2020.

Added features:
* Fully Bootstrap4 compatible
* Introduced OG:tags
* Introduced Schema tags.
* Has Estonian translations included

Known bugs:
* Product date picker throws an error. Needs tweaking

www.idyllum.com
www.elixir.ee
www.kookos.ee
